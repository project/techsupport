<?php
/**
 * @file
 * fe_tsm.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function fe_tsm_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_e228100b-79f5-415a-af59-d00b41c39bd6';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Issue node view',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'project_issue' => 'project_issue',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '91f35dbf-41d3-474d-a4c8-c10e3497c023';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2e0ec3ed-2930-47df-a90b-85a922e18fca';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'nodechanges-nodechanges_edit';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2e0ec3ed-2930-47df-a90b-85a922e18fca';
    $display->content['new-2e0ec3ed-2930-47df-a90b-85a922e18fca'] = $pane;
    $display->panels['bottom'][0] = 'new-2e0ec3ed-2930-47df-a90b-85a922e18fca';
    $pane = new stdClass();
    $pane->pid = 'new-78813880-91c4-4166-a525-61efce7f04f7';
    $pane->panel = 'left';
    $pane->type = 'node_body';
    $pane->subtype = 'node_body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Issue Description',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '78813880-91c4-4166-a525-61efce7f04f7';
    $display->content['new-78813880-91c4-4166-a525-61efce7f04f7'] = $pane;
    $display->panels['left'][0] = 'new-78813880-91c4-4166-a525-61efce7f04f7';
    $pane = new stdClass();
    $pane->pid = 'new-1041586d-d510-4090-9dac-509c2a3611c9';
    $pane->panel = 'left';
    $pane->type = 'node_comments';
    $pane->subtype = 'node_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'mode' => '0',
      'comments_per_page' => '50',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '1041586d-d510-4090-9dac-509c2a3611c9';
    $display->content['new-1041586d-d510-4090-9dac-509c2a3611c9'] = $pane;
    $display->panels['left'][1] = 'new-1041586d-d510-4090-9dac-509c2a3611c9';
    $pane = new stdClass();
    $pane->pid = 'new-cb5edd98-614d-4dcc-b8a4-4ea147129276';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'project_issue-issue-metadata';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cb5edd98-614d-4dcc-b8a4-4ea147129276';
    $display->content['new-cb5edd98-614d-4dcc-b8a4-4ea147129276'] = $pane;
    $display->panels['right'][0] = 'new-cb5edd98-614d-4dcc-b8a4-4ea147129276';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-78813880-91c4-4166-a525-61efce7f04f7';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_e228100b-79f5-415a-af59-d00b41c39bd6'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view__panel_context_c5246642-5d42-4ba7-99e3-6749cf776e33';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'My Account',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '57c0c065-6d92-4a6b-a3b5-d843344c2182';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f4a0e51b-afb7-4667-910d-5b1f3e076ecf';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'user_information';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'context' => array(
        0 => '',
      ),
      'override_title' => 1,
      'override_title_text' => 'User Information',
      'override_title_heading' => 'h3',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f4a0e51b-afb7-4667-910d-5b1f3e076ecf';
    $display->content['new-f4a0e51b-afb7-4667-910d-5b1f3e076ecf'] = $pane;
    $display->panels['middle'][0] = 'new-f4a0e51b-afb7-4667-910d-5b1f3e076ecf';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['user_view__panel_context_c5246642-5d42-4ba7-99e3-6749cf776e33'] = $handler;

  return $export;
}
