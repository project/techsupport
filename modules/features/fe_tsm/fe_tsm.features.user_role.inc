<?php
/**
 * @file
 * fe_tsm.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function fe_tsm_user_default_roles() {
  $roles = array();

  // Exported role: associate.
  $roles['associate'] = array(
    'name' => 'associate',
    'weight' => 3,
  );

  return $roles;
}
