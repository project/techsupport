<?php
/**
 * @file
 * fe_tsm.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fe_tsm_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_add-content:node/add.
  $menu_links['navigation_add-content:node/add'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(
      'identifier' => 'navigation_add-content:node/add',
      'alter' => TRUE,
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: navigation_issues:project/issues.
  $menu_links['navigation_issues:project/issues'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'project/issues',
    'router_path' => 'project/issues',
    'link_title' => 'Issues',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_issues:project/issues',
      'alter' => TRUE,
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add content');
  t('Issues');

  return $menu_links;
}
